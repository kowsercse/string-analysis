Requirements
--------------
* JDK 1.8
* Maven 3

How to run
-----------------
* To run unit tests only
  > mvn test
* To package
  > mvn clean install


Why do you like developing software?
--

It feels great to create something. It is fun to use logic, as I say organized thinking,
to develop a tool that helps people, improves usability.

I am not a poet or novelist. But to me developing software is comparable to writing a novel,
a fun of creation. The tremendous amount of thought which is embedded in a novel, is
comparable to software too. A number of people is using their thought and expressing it writing
code that ended up being a fully completed software, and creating positive ripple effect.
The fun is limitless.

And I believe I am good at it. So why not do it, the thing I like most.


Why are interfaces and protocols are important in software development and maintenance?
--

Interfaces needs to be treated as a contract. Whenever an interface is declared with specific
behaviours(methods), it means the interface is bound by those contract and all of its implementation
will be treated as same as long as these conforms to those behaviour. A simple example can be
List interface from Java. It has two different variant:
* ArrayList: Good for indexed data, bad for size change
* LinkedList: Good for size change, bad for indexed data

Java will treat both as List, but a programmer can use any of those according to requirement.
Or may be implement his own List interface if it requires. ImmutableList from java is one of that example.

Similarly there can be a Storage interface as below

> public interface Storage {
>   void store(File file);
> }

> public class AmazonCloudStorage extends Storage {
>   public void store(File file) { /* Store in Amazon cloud */ }
> }

> public class LocalStorage extends Storage {
>   public void store(File file) { /* Store in local hard drive */ }
> }

Now there can be different implementation AmazonCloudStorage that stores file into amazon cloud,
or a LocalStorage that stores files in local hard disk. But both of them will be treated as
storage. And program needs not to know which particular storage is being used.

Having interface makes it easy for third party integration, having different implementation to support different needs.


Protocols are standard in software. For example we have HTTP Protocol, REST Protocol, RSS Feed, etc.
Following protocols offers compatibility. Whenever an application says, it follows REST protocol,
it means the application use standard HTTP Header for response status, HTTP verbs like GET,
PUT, POST, etc. to modify resource. Now it is easy for third party to use its existing REST Client
to modify those resources.

Another example can be RSS Feed/Atom Protocol. Whenever a news/blogging web site exposes its news
content using this protocol any RSS Feed supported client can grab content from those web sites
without modifying their code or any internal behaviour.

Following protocol means conforming to industry standard, or writing a proprietary standard which
is easy for maintenance and third party integration in the long run.
