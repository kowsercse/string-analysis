package com.kowsercse;

import org.junit.*;

public class StringAnalyzerTestCase {

  private StringAnalyzer stringAnalyzer;

  @Before
  public void setUp() {
    stringAnalyzer = new StringAnalyzer();
  }

  @Test
  public void testLeastNumericStringSimple() {
    final String actual = stringAnalyzer.getLeastNumericString("HELLO", "H3110");
    Assert.assertEquals("Wrong numeric string for different numeric length", "HELLO", actual);
  }

  @Test
  public void testWithSameString() {
    final String actual = stringAnalyzer.getLeastNumericString("H3110", "H3110");
    Assert.assertEquals("Wrong numeric calculation for same string", "H3110", actual);
  }

  @Test
  public void testEqualNumericDifferentLength() {
    final String actual = stringAnalyzer.getLeastNumericString("h3ll0 world", "h3ll0");
    Assert.assertEquals("Large string should be returned for same numeric count", "h3ll0 world", actual);
  }

  @Test
  public void testLeastNumericStringEqualNumeric() {
    final String actual = stringAnalyzer.getLeastNumericString("h3110", "H3110");
    Assert.assertEquals("Wrong numeric string", "H3110", actual);
  }
}
