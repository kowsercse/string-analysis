package com.kowsercse;

import java.util.Objects;

public class StringAnalyzer {

  /**
   * @param first The first string parameter
   * @param second The second string parameter
   * @return the string with the least amount of numeric character
   */
  public String getLeastNumericString(final String first, final String second) {
    Objects.requireNonNull(first, "First string parameter should not be null");
    Objects.requireNonNull(second, "Second string parameter should not be null");

    int firstTotalNumeric = countNumericCharacter(first);
    int secondTotalNumeric = countNumericCharacter(second);

    if(firstTotalNumeric < secondTotalNumeric) {
      return first;
    }
    else if(firstTotalNumeric > secondTotalNumeric) {
      return second;
    }
    else {
      if (first.length() > second.length()) {
        return first;
      }
      else if (first.length() < second.length()) {
        return second;
      }
      else {
        return first.compareTo(second) >= 0 ? second : first;
      }
    }
  }

  private int countNumericCharacter(final String string) {
    int totalNumericCharacter = 0;
    for (int i = 0; i < string.length(); i++) {
      final char currentCharacter = string.charAt(i);
      if(currentCharacter >= '0' && currentCharacter <= '9') {
        totalNumericCharacter++;
      }
    }
    return totalNumericCharacter;
  }

}
